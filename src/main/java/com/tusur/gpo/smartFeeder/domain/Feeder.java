package com.tusur.gpo.smartFeeder.domain;

import lombok.*;

import javax.persistence.*;

@Entity
@Table(name = "feeder")
@Getter
@Setter
@ToString
@NoArgsConstructor
public class Feeder {

    @Id
    @Column(name = "id", unique = true, nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @NonNull
    private Long id;

    @Column(name = "name", unique = true, nullable = false)
    @NonNull
    private String name;

    @OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinColumn(name = "schedule_id", referencedColumnName = "id")
    private Schedule schedule;
}
