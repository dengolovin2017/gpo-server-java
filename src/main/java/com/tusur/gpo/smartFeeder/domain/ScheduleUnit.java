package com.tusur.gpo.smartFeeder.domain;

import lombok.*;

import javax.persistence.*;
import java.util.Date;


@Entity
@Table(name = "schedule_unit")
@Getter
@Setter
@ToString
@NoArgsConstructor
public class ScheduleUnit {

    @Id
    @Column(name = "id", unique = true, nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @NonNull
    private Long id;

    @Column(name = "time", nullable = false)
    @Temporal(TemporalType.TIME)
    @NonNull
    private Date time;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "schedule_id")
    private Schedule schedule;
}
