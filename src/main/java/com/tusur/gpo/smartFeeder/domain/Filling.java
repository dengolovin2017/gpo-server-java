package com.tusur.gpo.smartFeeder.domain;

import lombok.*;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "filling")
@Getter
@Setter
@ToString
@NoArgsConstructor
public class Filling {

    @Id
    @Column(name = "id", unique = true, nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @NonNull
    private Long id;

    @Column(name = "timestamp", columnDefinition = "TIMESTAMP WITHOUT TIME ZONE", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    @NonNull
    private Date timestamp;

    @Column(name = "gram_amount", nullable = false)
    @NonNull
    private Integer gramAmount;

    @ManyToOne
    @JoinColumn(name = "feeder_id")
    private Feeder feeder;
}
