package com.tusur.gpo.smartFeeder.domain;

import lombok.*;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "statistics")
@Getter
@Setter
@ToString
@NoArgsConstructor
public class Statistics {

    @Id
    @Column(name = "id", unique = true, nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @NonNull
    private Long id;

    @Column(name = "timestamp", columnDefinition = "TIMESTAMP WITHOUT TIME ZONE", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    @NonNull
    private Date timestamp;

    @Column(name = "eaten_gram", nullable = false)
    @NonNull
    private Integer eatenGram;

    @ManyToOne
    @JoinColumn(name = "feeder_id")
    private Feeder feeder;
}
