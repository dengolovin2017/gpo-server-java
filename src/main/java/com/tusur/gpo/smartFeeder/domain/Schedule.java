package com.tusur.gpo.smartFeeder.domain;

import lombok.*;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "schedule")
@Getter
@Setter
@ToString
@NoArgsConstructor
public class Schedule {

    @Id
    @Column(name = "id", unique = true, nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @NonNull
    private Long id;

    @Column(name = "name", unique = true, nullable = false)
    @NonNull
    private String name;

    @Column(name = "selected", nullable = false)
    @NonNull
    private Boolean selected;

    @OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinColumn(name = "feeder_id", referencedColumnName = "id")
    private Feeder feeder;

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "schedule")
    private List<ScheduleUnit> units;
}
