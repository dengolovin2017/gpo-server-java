package com.tusur.gpo.smartFeeder.repository;

import com.tusur.gpo.smartFeeder.domain.Feeder;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FeederRepo extends JpaRepository<Feeder, Long> {
}
