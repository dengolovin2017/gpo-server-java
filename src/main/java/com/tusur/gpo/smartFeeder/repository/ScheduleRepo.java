package com.tusur.gpo.smartFeeder.repository;

import com.tusur.gpo.smartFeeder.domain.Filling;
import com.tusur.gpo.smartFeeder.domain.Schedule;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ScheduleRepo extends JpaRepository<Schedule, Long> {


    @Query(value = "select s from Schedule s where s.feeder.id = :feederId and s.selected = true")
    Schedule findSelectedByFeederId(Long feederId);

    @Query(value = "select s from Schedule s where s.feeder.id = :feederId")
    List<Schedule> findAllByFeederId(Long feederId);

    @Query(value = "select count(s) from Schedule s where s.feeder.id = :feederId")
    Integer findCountByFeederId(Long feederId);
}
