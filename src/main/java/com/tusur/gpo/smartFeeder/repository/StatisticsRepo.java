package com.tusur.gpo.smartFeeder.repository;

import com.tusur.gpo.smartFeeder.domain.Statistics;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface StatisticsRepo extends JpaRepository<Statistics, Long> {


    @Query(value = "select s from Statistics s where s.feeder.id = :feederId")
    List<Statistics> findAllById(Long feederId);
}
