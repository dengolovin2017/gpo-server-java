package com.tusur.gpo.smartFeeder.repository;

import com.tusur.gpo.smartFeeder.domain.ScheduleUnit;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ScheduleUnitRepo extends JpaRepository<ScheduleUnit, Long> {
}
