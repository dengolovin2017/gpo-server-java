package com.tusur.gpo.smartFeeder.repository;

import com.tusur.gpo.smartFeeder.domain.Filling;
import com.tusur.gpo.smartFeeder.domain.Statistics;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface FillingRepo extends JpaRepository<Filling, Long> {

    @Query(value = "select f from Filling f where f.feeder.id = :feederId and f.id " +
            "in (select max(f2.id) from Filling f2 group by f2.feeder.id)")
    Filling findLast(Long feederId);

    @Query(value = "select f from Filling f where f.feeder.id = :feederId")
    List<Filling> findAllById(Long feederId);
}
