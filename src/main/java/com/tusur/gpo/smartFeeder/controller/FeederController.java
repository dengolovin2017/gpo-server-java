package com.tusur.gpo.smartFeeder.controller;

import com.tusur.gpo.smartFeeder.domain.Schedule;
import com.tusur.gpo.smartFeeder.domain.ScheduleUnit;
import com.tusur.gpo.smartFeeder.dto.ScheduleDto;
import com.tusur.gpo.smartFeeder.dto.ScheduleUnitDto;
import com.tusur.gpo.smartFeeder.service.FillingService;
import com.tusur.gpo.smartFeeder.service.ScheduleService;
import com.tusur.gpo.smartFeeder.service.StatisticsService;
import com.tusur.gpo.smartFeeder.web.packet.FeederPacket;
import com.tusur.gpo.smartFeeder.web.packet.SchedulePacket;
import com.tusur.gpo.smartFeeder.web.packet.StatisticsPacket;
import com.tusur.gpo.smartFeeder.web.response.FillingResponse;
import com.tusur.gpo.smartFeeder.web.response.ScheduleResponse;
import liquibase.repackaged.org.apache.commons.lang3.exception.ExceptionUtils;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;


@RequiredArgsConstructor
@Validated
@RequestMapping(path = "/api/feeder", produces = MediaType.APPLICATION_JSON_VALUE)
@RestController
public class FeederController {

    private final StatisticsService statisticsService;
    private final FillingService fillingService;
    private final ScheduleService scheduleService;

    @GetMapping("/statistics/{feederId}")
    public ResponseEntity<?> getStatistics(@PathVariable Long feederId) {
        try {
            StatisticsPacket packet = statisticsService.getPacket(feederId);
            return ResponseEntity.ok(packet);
        } catch (Exception e) {
            return processError("failed to receive StatisticsPacket", e);
        }
    }

    @GetMapping("/fillingAll/{feederId}")
    public ResponseEntity<?> getFillingAll(@PathVariable Long feederId) {
        try {
            FeederPacket packet = fillingService.getPacket(feederId);
            return ResponseEntity.ok(packet);
        } catch (Exception e) {
            return processError("failed to receive FeederPacket", e);
        }
    }

    @GetMapping("/fillingCurrent/{feederId}")
    public ResponseEntity<?> getFillingCurrent(@PathVariable Long feederId) {
        try {
            FillingResponse response = fillingService.getResponse(feederId);
            return ResponseEntity.ok(response);
        } catch (Exception e) {
            return processError("failed to receive FillingResponse", e);
        }
    }

    @GetMapping("/scheduleSelected/{feederId}")
    public ResponseEntity<?> getScheduleSelected(@PathVariable Long feederId) {
        try {
            ScheduleResponse response = scheduleService.getSelectedSchedule(feederId);
            return ResponseEntity.ok(response);
        } catch (Exception e) {
            return processError("failed to receive ScheduleResponse", e);
        }
    }

    @GetMapping("/scheduleAll/{feederId}")
    public ResponseEntity<?> getScheduleAll(@PathVariable Long feederId) {
        try {
            SchedulePacket packet = scheduleService.getSelectedAll(feederId);
            return ResponseEntity.ok(packet);
        } catch (Exception e) {
            return processError("failed to receive SchedulePacket", e);
        }
    }

    @PostMapping("/schedule/create")
    public ResponseEntity<?> createSchedule(ScheduleDto dto) {
        try {
            Schedule schedule = scheduleService.createSchedule(dto);
            return ResponseEntity.ok("created - " + schedule.getId());
        } catch (Exception e) {
            return processError("failed to receive SchedulePacket", e);
        }
    }

    @PostMapping("/scheduleUnit/create")
    public ResponseEntity<?> createScheduleUnit(ScheduleUnitDto dto) {
        try {
            ScheduleUnit unit = scheduleService.createScheduleUnit(dto);
            return ResponseEntity.ok("created - " + unit.getId());
        } catch (Exception e) {
            return processError("failed to receive SchedulePacket", e);
        }
    }

    @PutMapping("/schedule/update/{scheduleId}")
    public ResponseEntity<?> updateSchedule(@PathVariable Long scheduleId, ScheduleDto dto) {
        try {
            Schedule schedule = scheduleService.updateSchedule(scheduleId, dto);
            return ResponseEntity.ok("updated - " + schedule.getId());
        } catch (Exception e) {
            return processError("failed to receive SchedulePacket", e);
        }
    }

    @PutMapping("/scheduleUnit/update/{scheduleUnitId}")
    public ResponseEntity<?> createScheduleUnit(@PathVariable Long scheduleUnitId, ScheduleUnitDto dto) {
        try {
            ScheduleUnit unit = scheduleService.updateScheduleUnit(scheduleUnitId, dto);
            return ResponseEntity.ok("updated - " + unit);
        } catch (Exception e) {
            return processError("failed to receive SchedulePacket", e);
        }
    }

    @PatchMapping("/schedule/select/{scheduleId}")
    ResponseEntity<?> selectSchedule(@PathVariable Long scheduleId, Long feederId) {
        try {
            Schedule schedule = scheduleService.selectSchedule(scheduleId, feederId);
            return ResponseEntity.ok("selected - " + schedule);
        } catch (Exception e) {
            return processError("failed to receive SchedulePacket", e);
        }
    }

    public static ResponseEntity<?> processError(String error, Throwable th) {
        return ResponseEntity.badRequest().body(error + " Reason: " + ExceptionUtils.getMessage(th));
    }
}
