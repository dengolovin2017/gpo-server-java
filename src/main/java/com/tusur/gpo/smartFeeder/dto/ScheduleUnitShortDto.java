package com.tusur.gpo.smartFeeder.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import java.util.Date;

@Data
@AllArgsConstructor
public class ScheduleUnitShortDto {

    private Date time;
}