package com.tusur.gpo.smartFeeder.dto;

import lombok.Data;

import java.util.List;

@Data
public class ScheduleDto {

    private Long feederId;
    private String name;
    private boolean selected;
//    private List<ScheduleUnitDto> units;
}
