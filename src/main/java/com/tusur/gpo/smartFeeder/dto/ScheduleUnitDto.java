package com.tusur.gpo.smartFeeder.dto;

import lombok.Data;

import java.util.Date;

@Data
public class ScheduleUnitDto {

    private Long scheduleId;
    private Date time;
}
