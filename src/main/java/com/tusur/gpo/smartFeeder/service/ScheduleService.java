package com.tusur.gpo.smartFeeder.service;

import com.tusur.gpo.smartFeeder.domain.Schedule;
import com.tusur.gpo.smartFeeder.domain.ScheduleUnit;
import com.tusur.gpo.smartFeeder.dto.ScheduleDto;
import com.tusur.gpo.smartFeeder.dto.ScheduleUnitDto;
import com.tusur.gpo.smartFeeder.dto.ScheduleUnitShortDto;
import com.tusur.gpo.smartFeeder.repository.FeederRepo;
import com.tusur.gpo.smartFeeder.repository.ScheduleRepo;
import com.tusur.gpo.smartFeeder.repository.ScheduleUnitRepo;
import com.tusur.gpo.smartFeeder.web.packet.SchedulePacket;
import com.tusur.gpo.smartFeeder.web.response.ScheduleResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class ScheduleService {

    private final ScheduleRepo scheduleRepo;
    private final ScheduleUnitRepo scheduleUnitRepo;
    private final FeederRepo feederRepo;

    public ScheduleResponse getSelectedSchedule(Long feederId) {
        return getScheduleResponse(scheduleRepo.findSelectedByFeederId(feederId));
    }

    public SchedulePacket getSelectedAll(Long feederId) {
        SchedulePacket packet = new SchedulePacket();
        packet.setResponses(scheduleRepo.findAllByFeederId(feederId).stream().map(this::getScheduleResponse).collect(Collectors.toList()));
        return packet;
    }

    @Transactional
    public Schedule createSchedule(ScheduleDto dto) {

        if (dto.isSelected()) {
            Schedule schedule = scheduleRepo.findSelectedByFeederId(dto.getFeederId());
            schedule.setSelected(false);
        }
        return saveSchedule(dto);
    }

    @Transactional
    public Schedule updateSchedule(Long scheduleId, ScheduleDto dto) {

        Schedule schedule = scheduleRepo.getReferenceById(scheduleId);
        schedule.setName(dto.getName());
        schedule.setSelected(dto.isSelected());
        schedule.setFeeder(feederRepo.getReferenceById(dto.getFeederId()));

        return scheduleRepo.save(schedule);
    }

    @Transactional
    public ScheduleUnit createScheduleUnit(ScheduleUnitDto dto) {

        ScheduleUnit unit = new ScheduleUnit();
        unit.setSchedule(scheduleRepo.getReferenceById(dto.getScheduleId()));
        unit.setTime(dto.getTime());

        return scheduleUnitRepo.save(unit);
    }

    @Transactional
    public ScheduleUnit updateScheduleUnit(Long scheduleUnitId, ScheduleUnitDto dto) {

        ScheduleUnit unit = scheduleUnitRepo.getReferenceById(scheduleUnitId);
        unit.setSchedule(scheduleRepo.getReferenceById(dto.getScheduleId()));
        unit.setTime(dto.getTime());

        return scheduleUnitRepo.save(unit);
    }

    @Transactional
    public Schedule selectSchedule(Long scheduleId, Long feederId) {

        Schedule schedule = scheduleRepo.findSelectedByFeederId(feederId);
        schedule.setSelected(false);
        scheduleRepo.save(schedule);

        Schedule selectSchedule = scheduleRepo.getReferenceById(scheduleId);
        selectSchedule.setSelected(true);
        return scheduleRepo.save(selectSchedule);
    }




    @Transactional
    Schedule saveSchedule(ScheduleDto dto) {
        Schedule schedule = new Schedule();
        schedule.setName(dto.getName());
        schedule.setSelected(dto.isSelected());
        schedule.setFeeder(feederRepo.getReferenceById(dto.getFeederId()));

//        List<ScheduleUnit> units = new ArrayList<>();
//        for (ScheduleUnitDto unit: dto.getUnits()) {
//            ScheduleUnit scheduleUnit = new ScheduleUnit();
//            scheduleUnit.setTime(unit.getTime());
//            scheduleUnit.setSchedule(schedule);
//            units.add(scheduleUnitRepo.save(scheduleUnit));
//        }
//        schedule.setUnits(units);
        return scheduleRepo.save(schedule);
    }

    private ScheduleResponse getScheduleResponse(Schedule schedule) {
        ScheduleResponse response = new ScheduleResponse();
        response.setId(schedule.getId());
        response.setFeederId(schedule.getFeeder().getId());
        response.setFeederName(schedule.getFeeder().getName());
        response.setName(schedule.getName());
        response.setSelected(schedule.getSelected());
        List<ScheduleUnitShortDto> shortDtos = schedule.getUnits().stream().map(s -> new ScheduleUnitShortDto(s.getTime())).toList();
        response.setUnits(shortDtos);
        return response;
    }



}
