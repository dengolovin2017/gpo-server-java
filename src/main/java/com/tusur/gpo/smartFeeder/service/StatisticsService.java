package com.tusur.gpo.smartFeeder.service;

import com.tusur.gpo.smartFeeder.repository.StatisticsRepo;
import com.tusur.gpo.smartFeeder.web.packet.StatisticsPacket;
import com.tusur.gpo.smartFeeder.web.response.StatisticsResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public record StatisticsService(StatisticsRepo statisticsRepo) {

    public StatisticsPacket getPacket(Long feederId) {

        List<StatisticsResponse> responses = statisticsRepo.findAllById(feederId).stream().map(s -> {
            StatisticsResponse response = new StatisticsResponse();
            response.setId(s.getId());
            response.setFeederId(s.getFeeder().getId());
            response.setFeederName(s.getFeeder().getName());
            response.setTimestamp(s.getTimestamp());
            response.setEatenGram(s.getEatenGram());
            return response;
        }).collect(Collectors.toList());

        StatisticsPacket packet = new StatisticsPacket();
        packet.setResponses(responses);
        return packet;
    }
}
