package com.tusur.gpo.smartFeeder.service;

import com.tusur.gpo.smartFeeder.domain.Filling;
import com.tusur.gpo.smartFeeder.repository.FillingRepo;
import com.tusur.gpo.smartFeeder.web.packet.FeederPacket;
import com.tusur.gpo.smartFeeder.web.response.FillingResponse;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public record FillingService(FillingRepo fillingRepo) {

    public FeederPacket getPacket(Long feederId) {

        List<FillingResponse> responses = fillingRepo.findAllById(feederId)
                .stream().map(this::getFillingResponse).collect(Collectors.toList());

        FeederPacket packet = new FeederPacket();
        packet.setResponses(responses);
        return packet;
    }

    public FillingResponse getResponse(Long feederId) {
        Filling filling = fillingRepo.findLast(feederId);
        return getFillingResponse(filling);
    }

    private FillingResponse getFillingResponse(Filling filling) {
        FillingResponse response = new FillingResponse();
        response.setId(filling.getId());
        response.setFeederId(filling.getFeeder().getId());
        response.setFeederName(filling.getFeeder().getName());
        response.setTimestamp(filling.getTimestamp());
        response.setGramAmount(filling.getGramAmount());

        return response;
    }
}
