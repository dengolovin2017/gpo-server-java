package com.tusur.gpo.smartFeeder.web.response;

import com.tusur.gpo.smartFeeder.domain.ScheduleUnit;
import com.tusur.gpo.smartFeeder.dto.ScheduleUnitShortDto;
import lombok.Data;

import java.util.List;

@Data
public class ScheduleResponse {

    private Long id;
    private Long feederId;
    private String feederName;
    private String name;
    private boolean selected;
    private List<ScheduleUnitShortDto> units;

}
