package com.tusur.gpo.smartFeeder.web.packet;

import com.tusur.gpo.smartFeeder.web.response.ScheduleResponse;
import lombok.Data;

import java.util.List;

@Data
public class SchedulePacket {

    private List<ScheduleResponse> responses;
}
