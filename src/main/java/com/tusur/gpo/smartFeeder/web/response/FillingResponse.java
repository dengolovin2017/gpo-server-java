package com.tusur.gpo.smartFeeder.web.response;

import lombok.Data;

import java.util.Date;

@Data
public class FillingResponse {

    private Long id;
    private Long feederId;
    private String feederName;
    private Date timestamp;
    private Integer gramAmount;
}
