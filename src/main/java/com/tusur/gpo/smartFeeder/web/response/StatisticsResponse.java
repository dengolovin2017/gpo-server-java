package com.tusur.gpo.smartFeeder.web.response;

import lombok.Data;

import java.util.Date;

@Data
public class StatisticsResponse {

    private Long id;
    private String feederName;
    private Long feederId;
    private Date timestamp;
    private Integer eatenGram;
}
