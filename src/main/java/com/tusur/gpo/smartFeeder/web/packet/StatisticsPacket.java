package com.tusur.gpo.smartFeeder.web.packet;

import com.tusur.gpo.smartFeeder.web.response.StatisticsResponse;
import lombok.Data;

import java.util.List;

@Data
public class StatisticsPacket {

    private List<StatisticsResponse> responses;
}
