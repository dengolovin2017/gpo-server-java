package com.tusur.gpo.smartFeeder.web.packet;

import com.tusur.gpo.smartFeeder.web.response.FillingResponse;
import lombok.Data;

import java.util.List;

@Data
public class FeederPacket {

    private List<FillingResponse> responses;
}