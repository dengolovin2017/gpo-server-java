package com.tusur.gpo.smartFeeder;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@EntityScan
@EnableJpaRepositories
public class SmartFeederApplication {

	public static void main(String[] args) {
		SpringApplication.run(SmartFeederApplication.class, args);
	}

}
